# This file is a template, and might need editing before it works on your project.
FROM openjdk:8-jdk-alpine

#Enviroment variables for h2 database and JAVA_OPTS
ENV DB_profile h2
ENV JAVA_OPTS "-Xms64m -Xmx128m"

EXPOSE 8070

#Copying the jar file and specify the working directory 
COPY  /target/*.jar /app/assignment.jar

WORKDIR /app

CMD java -jar $JAVA_OPTS -Dserver.port=8070 -Dspring.profiles.active=$DB_profile assignment.jar
